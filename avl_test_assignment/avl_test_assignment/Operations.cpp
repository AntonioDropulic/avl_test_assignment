#include "Operations.h"

#include <cmath>
#include <algorithm>

Identity::Identity() : IOperation<double>{}, IStringable{ "Identity" } {}

double Identity::operator()(double x) const
{
	return x;
}

std::string Identity::toString() const
{
	return IStringable::toString();
}

Addition::Addition(double x) 
	: IOperation<double>{}, IStringable{ "Addition" }, val(x) {}

double Addition::operator()(double x) const
// precondition f(val, x) : double -> double
// possible overflow
{
	return x + val;
}

std::string Addition::toString() const
{
	return IStringable::toString() + '(' + std::to_string(val) + ')';
}

Multiplication::Multiplication(double x) 
	: IOperation<double>{}, IStringable{ "Multiplication" }, val(x) {}

double Multiplication::operator()(double x) const
// precondition f(val, x) : double -> double
// possible overflow
{
	return x * val;
}

std::string Multiplication::toString() const
{
	return IStringable::toString() + '(' + std::to_string(val) + ')';
}

Power::Power(int exponent)
	: IOperation<double>{}, IStringable{ "Power" }, val(exponent) 
{
}

double Power::operator()(double x) const
// precondition f(val, x) : double -> double
// possible overflow
{
	return std::pow(x, val);
}

std::string Power::toString() const
{
	return IStringable::toString() + '(' + std::to_string(val) + ')';
}

Limit::Limit(double x, double y)
	: IOperation<double>{}, IStringable{ "Limit" }, lowerBound(x), upperBound(y) {}

double Limit::operator()(double x) const
{
	return std::clamp(x, lowerBound, upperBound);
}

std::string Limit::toString() const
{
	return IStringable::toString() + '(' + std::to_string(lowerBound) + ", " + std::to_string(upperBound) + ')';
}

Condition::Condition(double x)
	: IOperation<double>{}, IStringable{ "Condition" }, val(x) {}

// C++ 20 <=> ?
double Condition::operator()(double x) const
{
	if (x < val)
		return -1;
	else if (x == val)
		return 0;
	else
		return 1;
}

std::string Condition::toString() const
{
	return IStringable::toString() + '(' + std::to_string(val) + ')';
}



IStringable::IStringable(std::string name) : name(name) {}

std::string IStringable::toString() const
{
	return name;
}

std::ostream & operator<<(std::ostream& os, const IStringable & x)
{
	return os << x.toString();
}
