#include "OperationSequence.h"
#include <algorithm>
#include <fstream>
#include <iterator>
#include <iostream>

void OperationSequence::appendOperation(std::unique_ptr<IOperation<double>> operation)
// memory alloc exception, otherwise no throw
{
	operation_sequence.push_back(std::move(operation));
}

void OperationSequence::moveOperation(int operation_index)
// precondition: 0 < operation_index < operation_sequence.size
{
	auto it = std::begin(operation_sequence) + operation_index;
	std::rotate(std::begin(operation_sequence), it, it + 1);
}


void OperationSequence::eraseOperation(int operation_index)
// precondition: 0 < operation_index < operation_sequence.size
{
	operation_sequence.erase(std::begin(operation_sequence) + operation_index);
}

const std::vector<std::unique_ptr<IOperation<double>>>& OperationSequence::data() const
{
	return operation_sequence;
}



double executeSequence(const OperationSequence& operation_sequence, double value)
{
	for (auto& operation : operation_sequence.data())
	{
		value = (*operation)(value);
	}

	return value;
}

void executeSequence(const OperationSequence& operation_sequence, const std::string& file_name)
// precondition: file contains a sequence of space delimited doubles / convertible to double
//				 file with file_name exists
{
	std::ofstream output_file("output_file.txt");
	std::ifstream input_file(file_name);

	// consider renaming execute sequence to avoid overload resolution problems
	// also quite possibly breaking lsp?
	std::transform(
		std::istream_iterator<double>(input_file), std::istream_iterator<double>(),
		std::ostream_iterator<double>(output_file, " "),
		[&operation_sequence](const double& x) {return executeSequence(operation_sequence, x); }
	);
}

void printOperationSequence(const OperationSequence & operation, std::ostream & os)
// requirement sequence is IStringable
{
	for (auto& element : operation.data())
	{
		auto stringable = dynamic_cast<const IStringable*>(element.get());
		if (stringable)
			std::cout << *stringable << " ";
		else
			std::cout << "Non Stringable Operation ";
	}
	std::cout << "\n";
}
