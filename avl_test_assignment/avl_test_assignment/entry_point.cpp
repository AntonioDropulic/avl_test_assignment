#include "OperationSequence.h"
#include "Operations.h"
#include "UserInterface.h"

int main()
{
	MenuControler menu{ std::make_unique<MainMenu>(), std::make_unique<OperationSequence>()};
	menu.operation_sequence->appendOperation(std::make_unique<Addition>(3));

	// TODO: Program exit
	while (true)
	{
		system("CLS");
		menu.render();
		menu.waitUserInput();
		menu.update();
	}
		
	

}
