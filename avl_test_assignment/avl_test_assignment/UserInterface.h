#pragma once
#include <memory>
#include <string>
#include <array>

class OperationSequence;
// will move to menu FSM
class IMenu;
class MenuControler
{
public:
	// test copy construction
	// change to generic initialization
	MenuControler(std::unique_ptr<IMenu> main_menu, std::unique_ptr<OperationSequence> operation_sequence);

	// add constness
	void render();
	void waitUserInput();
	void update();

	// State and the model can be changed by anyone with access to the MenuControler
	// Proposal:
	// IMenu can be a friend and implement functions change state and offer indirect model(operation sequence) interface access 
	// Requires IMenu to know about Models inteface
	// Downsides are it being convoluted, losing option for a generic Imenu, MenuControler pair, 
	// Think how to avoid coupling while still making sure user has sole ownership of the model
	void changeState(std::unique_ptr<IMenu> menu);
	std::unique_ptr<OperationSequence> operation_sequence;
private:
	std::unique_ptr<IMenu>  current_menu;
};


// will move to menus
class IMenu
{
public:
	virtual void render(MenuControler& menu_controler) const = 0;
	virtual void waitUserInput(MenuControler& menu_controler) = 0;
	virtual void update(MenuControler&) = 0;

	virtual ~IMenu() = default;
protected:
	bool inputValidated{ false };
};

class MainMenu : public IMenu
{
public:
	void render(MenuControler& menu_controler) const override;
	void waitUserInput(MenuControler& menu_controler) override;
	void update(MenuControler& menu_controler) override;
private:
	int input;
};

class AppendMenu : public IMenu
{
public:
	void render(MenuControler& menu_controler) const override;
	void waitUserInput(MenuControler& menu_controler) override;
	void update(MenuControler& menu_controler) override;
private:
	int input;
};

class MoveMenu : public IMenu
{
public:
	void render(MenuControler& menu_controler) const override;
	void waitUserInput(MenuControler& menu_controler) override;
	void update(MenuControler& menu_controler) override;
private:
	int input;
};

class EraseMenu : public IMenu
{
public:
	void render(MenuControler& menu_controler) const override;
	void waitUserInput(MenuControler& menu_controler) override;
	void update(MenuControler& menu_controler) override;
private:
	int input;
};

class PrintMenu : public IMenu
{
public:
	void render(MenuControler& menu_controler) const override;
	void waitUserInput(MenuControler& menu_controler) override;
	void update(MenuControler& menu_controler) override;
};

class ExecuteOnValueMenu : public IMenu
{
public:
	void render(MenuControler& menu_controler) const override;
	void waitUserInput(MenuControler& menu_controler) override;
	void update(MenuControler& menu_controler) override;
private:
	double input;
};

class ExecuteOnFileMenu : public IMenu
{
public:
	void render(MenuControler& menu_controler) const override;
	void waitUserInput(MenuControler& menu_controler) override;
	void update(MenuControler& menu_controler) override;
private:
	std::string input;
};

class IdentityMenu : public IMenu
{
public:
	void render(MenuControler& menu_controler) const override;
	void waitUserInput(MenuControler& menu_controler) override;
	void update(MenuControler& menu_controler) override;
};

class AdditionMenu : public IMenu
{
public:
	void render(MenuControler& menu_controler) const override;
	void waitUserInput(MenuControler& menu_controler) override;
	void update(MenuControler& menu_controler) override;
private:
	double input;
};

class MultiplicationMenu : public IMenu
{
public:
	void render(MenuControler& menu_controler) const override;
	void waitUserInput(MenuControler& menu_controler) override;
	void update(MenuControler& menu_controler) override;
private:
	double input;
};

class PowerMenu : public IMenu
{
public:
	void render(MenuControler& menu_controler) const override;
	void waitUserInput(MenuControler& menu_controler) override;
	void update(MenuControler& menu_controler) override;
private:
	int input;
};

class LimitMenu : public IMenu
{
public:
	void render(MenuControler& menu_controler) const override;
	void waitUserInput(MenuControler& menu_controler) override;
	void update(MenuControler& menu_controler) override;
private:
	std::array<double, 2> input;
};

class ConditionMenu : public IMenu
{
public:
	void render(MenuControler& menu_controler) const override;
	void waitUserInput(MenuControler& menu_controler) override;
	void update(MenuControler& menu_controler) override;
private:
	double input;
};






