#pragma once
#include <string>

template<typename T>
class IOperation
{
public:
	virtual T operator()(T) const = 0;
	virtual ~IOperation() = default;
};

class IStringable
{
public:
	IStringable(std::string name);
	virtual std::string toString() const;
	virtual ~IStringable() = default;
private:
	const std::string name;
};

std::ostream& operator<<(std::ostream&, const IStringable& str);

struct Identity : public IOperation<double>, public IStringable
{
	Identity();
	double operator()(double x) const override;
	std::string toString() const override;
};

class Addition : public IOperation<double>, public IStringable
{
public:
	Addition(double x);
	double operator()(double x) const;
	std::string toString() const override;
private:
	const double val;
};

class Multiplication : public IOperation<double>, public IStringable
{
public:
	Multiplication(double x);
	double operator()(double x) const;
	std::string toString() const override;
private:
	const double val;
};

class Power : public IOperation<double>, public IStringable
{
public:
	Power(int exponent);
	double operator()(double x) const;
	std::string toString() const override;
private:
	const int val;
};

class Limit : public IOperation<double>, public IStringable
{
public:
	Limit(double x, double y);
	double operator()(double x) const;
	std::string toString() const override;
private:
	const double lowerBound;
	const double upperBound;
};

class Condition : public IOperation<double>, public IStringable
{
public:
	Condition(double x);
	double operator()(double x) const;
	std::string toString() const override;
private:
	const double val;
};