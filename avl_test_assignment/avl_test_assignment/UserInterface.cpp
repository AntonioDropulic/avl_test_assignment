#include "UserInterface.h"
#include "OperationSequence.h"

#include <iostream>


// move to a more appropriate location
#include <regex>
bool isIntegerInRange(const std::string& str, int from, int to)
{
	std::regex regex_pattern("[" + std::to_string(from) + "-" + std::to_string(to) + "]");	
	return std::regex_match(str, regex_pattern);
}

bool isNonNegativeInteger(const std::string& str)
{
	std::regex regex_pattern("[0-9][0-9]*");
	return std::regex_match(str, regex_pattern);
}

bool isDouble(const std::string& str)
{
	std::regex regex_pattern("[-+]?[0-9]*\\.?[0-9]+");
	return std::regex_match(str, regex_pattern);
}

void MainMenu::render(MenuControler& menu_controler) const
{
	std::cout <<
		"Select sequence operation: \n"
		"1. Append\n"
		"2. Move\n"
		"3. Erase\n"
		"4. Print\n"
		"5. Execute on value\n"
		"6. Execute on file\n";
}

void MainMenu::waitUserInput(MenuControler& menu_controler)
{
	std::string input_string;
	std::getline(std::cin, input_string);
	inputValidated = isIntegerInRange(input_string, 1, 6);
	input = std::stoi(input_string);
}


void MainMenu::update(MenuControler& menu_controler)
{
	if (inputValidated)
	{
		switch (input) {
		case 1: menu_controler.changeState(std::make_unique<AppendMenu>());
			break;
		case 2: menu_controler.changeState(std::make_unique<MoveMenu>());
			break;
		case 3: menu_controler.changeState(std::make_unique<EraseMenu>());
			break;
		case 4: menu_controler.changeState(std::make_unique<PrintMenu>());
			break;
		case 5: menu_controler.changeState(std::make_unique<ExecuteOnValueMenu>());
			break;
		case 6: menu_controler.changeState(std::make_unique<ExecuteOnFileMenu>());
			break;
		default:;
		}
	}


}

void AppendMenu::render(MenuControler& menu_controler) const
{
	std::cout <<
		"Select operation to append: \n"
		"1. Identity\n"
		"2. Addition\n"
		"3. Multiplication\n"
		"4. Power\n"
		"5. Limit\n"
		"6. Condition\n";
}

void AppendMenu::waitUserInput(MenuControler& menu_controler)
{
	std::string input_string;
	std::getline(std::cin, input_string);
	inputValidated = isIntegerInRange(input_string, 1, 6);
	if (inputValidated)
		input = std::stoi(input_string);
}

void AppendMenu::update(MenuControler & menu_controler)
{
	if (inputValidated)
	{

	}
	switch (input) {
		case 1: menu_controler.changeState(std::make_unique<IdentityMenu>());
				break;
		case 2: menu_controler.changeState(std::make_unique<AdditionMenu>());
				break;
		case 3: menu_controler.changeState(std::make_unique<MultiplicationMenu>());
				break;
		case 4: menu_controler.changeState(std::make_unique<PowerMenu>());
				break;
		case 5: menu_controler.changeState(std::make_unique<LimitMenu>());
				break;
		case 6: menu_controler.changeState(std::make_unique<ConditionMenu>());
				break;
		default:;
	}
}

void MoveMenu::render(MenuControler& menu_controler) const
{
	int operation_sequence_size = menu_controler.operation_sequence->data().size();

	if (operation_sequence_size > 0)
	{
	std::cout << "Enter the index of the element you want to move to the start of the sequence.\n"
				 "Acceptable indices are: [0 - "
			  << operation_sequence_size - 1 << "].\n";
	}
	else
	{
		std::cout << "Sequence is empty. Press Enter to return to main menu.\n";
	}
	
}

void MoveMenu::waitUserInput(MenuControler& menu_controler)
// precondition input <= max_int
{
	int operation_sequence_size = menu_controler.operation_sequence->data().size();

	if (operation_sequence_size == 0)
	{
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}
	else
	{
		std::string input_string;
		std::getline(std::cin, input_string);

		if (isNonNegativeInteger(input_string))
		{
			input = std::stoi(input_string);
			// inputValidated = isNonNegativeInteger(input) && input < operation_sequence_size
			inputValidated = input < operation_sequence_size;
		}
	}
		
}

void MoveMenu::update(MenuControler & menu_controler)
{
	if(inputValidated)
	{
		menu_controler.operation_sequence->moveOperation(input);
		menu_controler.changeState(std::make_unique<MainMenu>());
	}
	else
	{
		menu_controler.changeState(std::make_unique<MainMenu>());
	}
}

void EraseMenu::render(MenuControler& menu_controler) const
{
	int operation_sequence_size = menu_controler.operation_sequence->data().size();
	
	if (operation_sequence_size > 0)
	{
		std::cout << "Enter the index of the element to erase.\n"
			"Acceptable indices are: [0 - "
			<< operation_sequence_size - 1 << "].\n";
	}
	else
	{
		std::cout << "Sequence is empty. Press Enter to return to main menu.\n";
	}
}

void EraseMenu::waitUserInput(MenuControler& menu_controler)
{
	int operation_sequence_size = menu_controler.operation_sequence->data().size();

	if (operation_sequence_size == 0)
	{
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}
	else
	{
		std::string input_string;
		std::getline(std::cin, input_string);

		if (isNonNegativeInteger(input_string))
		{
			input = std::stoi(input_string);
			// inputValidated = isNonNegativeInteger(input) && input < operation_sequence_size
			inputValidated = input < operation_sequence_size;
		}
	}
}

void EraseMenu::update(MenuControler & menu_controler)
{
	if (inputValidated)
	{
		menu_controler.operation_sequence->eraseOperation(input);
		menu_controler.changeState(std::make_unique<MainMenu>());
	}
	else
	{
		menu_controler.changeState(std::make_unique<MainMenu>());
	}	
}

void PrintMenu::render(MenuControler& menu_controler) const
{
	std::cout << "Current sequence: \n";
	printOperationSequence(*(menu_controler.operation_sequence), std::cout);
	std::cout << "Press Enter to return to main menu.\n";
}

void PrintMenu::waitUserInput(MenuControler& menu_controler)
{
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

void PrintMenu::update(MenuControler & menu_controler)
{
	
	menu_controler.changeState(std::make_unique<MainMenu>());
}

void ExecuteOnValueMenu::render(MenuControler& menu_controler) const
{
	std::cout << "Enter a real number:\n";
}

void ExecuteOnValueMenu::waitUserInput(MenuControler& menu_controler)
{
	std::string input_string;
	std::getline(std::cin, input_string);

	inputValidated = isDouble(input_string);
	if(inputValidated)
		input = std::stod(input_string);
}

void ExecuteOnValueMenu::update(MenuControler & menu_controler)
{
	if (inputValidated)
	{
		std::cout << executeSequence(*(menu_controler.operation_sequence), input) << '\n';

		// this should be a state ...
		// fix if there is time
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		menu_controler.changeState(std::make_unique<MainMenu>());
	}
}

// TODO: not implemented
void ExecuteOnFileMenu::render(MenuControler& menu_controler) const
{
	std::cout << "Press Enter to return to main menu.\n";
}

void ExecuteOnFileMenu::waitUserInput(MenuControler& menu_controler)
{

	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

void ExecuteOnFileMenu::update(MenuControler & menu_controler)
{
	menu_controler.changeState(std::make_unique<MainMenu>());
}


MenuControler::MenuControler(std::unique_ptr<IMenu> main_menu, std::unique_ptr<OperationSequence> operation_sequence)
	: current_menu(std::move(main_menu)), operation_sequence(std::move(operation_sequence))
{
}

void MenuControler::render() 
{
	current_menu->render(*this);
}

void MenuControler::waitUserInput()
{
	current_menu->waitUserInput(*this);
}

void MenuControler::update()
{
	current_menu->update(*this);
}

void MenuControler::changeState(std::unique_ptr<IMenu> menu)
{
	current_menu.reset(menu.release());
}

void IdentityMenu::render(MenuControler & menu_controler) const
{
	std::cout << "Identity needs no additional parameters. Press Enter to return to main menu.\n";
}

void IdentityMenu::waitUserInput(MenuControler & menu_controler)
{
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

void IdentityMenu::update(MenuControler & menu_controler)
{
	menu_controler.operation_sequence->appendOperation(std::make_unique<Identity>());
	menu_controler.changeState(std::make_unique<MainMenu>());
}

void AdditionMenu::render(MenuControler & menu_controler) const
{
	std::cout << "Enter a real number: \n";
}

void AdditionMenu::waitUserInput(MenuControler & menu_controler)
{
	std::string input_string;
	std::getline(std::cin, input_string);

	inputValidated = isDouble(input_string);
	if (inputValidated)
		input = std::stod(input_string);
}

void AdditionMenu::update(MenuControler & menu_controler)
{
	if (inputValidated)
	{
		menu_controler.operation_sequence->appendOperation(std::make_unique<Addition>(input));
		menu_controler.changeState(std::make_unique<MainMenu>());
	}
}

void MultiplicationMenu::render(MenuControler & menu_controler) const
{
	std::cout << "Enter a real number: \n";
}

void MultiplicationMenu::waitUserInput(MenuControler & menu_controler)
{
	std::string input_string;
	std::getline(std::cin, input_string);

	inputValidated = isDouble(input_string);
	if (inputValidated)
		input = std::stod(input_string);
}

void MultiplicationMenu::update(MenuControler & menu_controler)
{
	if (inputValidated)
	{
		menu_controler.operation_sequence->appendOperation(std::make_unique<Addition>(input));
		menu_controler.changeState(std::make_unique<MainMenu>());
	}
}

void PowerMenu::render(MenuControler & menu_controler) const
{
	std::cout << "Enter a non negative integer: \n";
}

void PowerMenu::waitUserInput(MenuControler & menu_controler)
{
	std::string input_string;
	std::getline(std::cin, input_string);

	inputValidated = isNonNegativeInteger(input_string);
	if(inputValidated)
		input = std::stoi(input_string);	
}

void PowerMenu::update(MenuControler & menu_controler)
{
	if (inputValidated)
	{
		menu_controler.operation_sequence->appendOperation(std::make_unique<Power>(input));
		menu_controler.changeState(std::make_unique<MainMenu>());
	}
}

void LimitMenu::render(MenuControler & menu_controler) const
{
	std::cout << "Enter two real numbers, in separate lines: \n";
}

void LimitMenu::waitUserInput(MenuControler & menu_controler)
{
	for (auto& element : input)
	{
		std::string input_string;
		std::getline(std::cin, input_string);

		inputValidated = isDouble(input_string);
		if (inputValidated)
			element = std::stod(input_string);
		else
			break;
	}
}

void LimitMenu::update(MenuControler & menu_controler)
{
	if (inputValidated)
	{
		menu_controler.operation_sequence->appendOperation(std::make_unique<Limit>(input[0], input[1]));
		menu_controler.changeState(std::make_unique<MainMenu>());
	}
}

void ConditionMenu::render(MenuControler & menu_controler) const
{
	std::cout << "Enter a real number: \n";
}

void ConditionMenu::waitUserInput(MenuControler & menu_controler)
{
	std::string input_string;
	std::getline(std::cin, input_string);

	inputValidated = isDouble(input_string);
	if (inputValidated)
		input = std::stod(input_string);
}

void ConditionMenu::update(MenuControler & menu_controler)
{
	if (inputValidated)
	{
		menu_controler.operation_sequence->appendOperation(std::make_unique<Condition>(input));
		menu_controler.changeState(std::make_unique<MainMenu>());
	}
}
