#pragma once

#include <vector>
#include <memory>
#include "Operations.h"


// template it to get generic operation sequences
class OperationSequence
//	class invariant: 
//		operations are immutable
//		only defined operations are allowed
{
public:
	// assuring sequence owns operation
	// not sure if the interface should be IOperation, or should it contain all the assignment functionalities for convenience
	void appendOperation(std::unique_ptr<IOperation<double>> operation);
	void moveOperation(int operation_index);
	void eraseOperation(int operation_index);

	// rethink removing
	const std::vector<std::unique_ptr<IOperation<double>>>& data() const;

private:
	std::vector<std::unique_ptr<IOperation<double>>> operation_sequence;
};

double executeSequence(const OperationSequence& operation_sequence, double value);
void executeSequence(const OperationSequence& operation_sequence, const std::string& file_name);


// op being a f: double -> double not enough
// requires type -> UID
void printOperationSequence(const OperationSequence& operation, std::ostream& os);

// TODO: IMPLEMENT
// requires type -> UID, member_val
void saveOperationSequence();
// requires UID -> type
void loadOperationSequence();